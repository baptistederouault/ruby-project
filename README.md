# Projet Hashi
### Projet de troisième année de licence informatique consitant en la création d'un hashi graphique grâce à ruby et à GTK. 

#### Par DEROUAULT Baptiste, CHAUVIN Lucien, DUBIN Baptiste, GIROD Valentin, MOTTIER Anaïs, RENARD Dorian, TSAMARAYEV Moustapha, ZHENG Haoran.

Hashi, branche principale {master} : <a href="https://gitlab.com/groupe-1_l3/ruby-project/-/archive/master/ruby-project-master.zip"><img alt="download icon" src="https://zuraiko.ml/Ressources/download.svg" /></a>  

Tout le code est situé dans la branche master.   

# Liens utiles

#### Documentation
<a href="https://documentation-hashi.zuraiko.fr/"> Documentation du projet - Hashi</a>.


# Attributions
<a href="https://www.gtk.org/"> Ensemble de bibliothèques logicielles- gtk.org</a>.
<div>Icons made by <a href="https://www.freepik.com" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>  
Toutes les licences sont disponibles dans le dossier LEGAL.

